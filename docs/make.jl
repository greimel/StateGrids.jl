using Documenter, StateGrids

makedocs(;
    modules=[StateGrids],
    format=Documenter.HTML(),
    pages=[
        "Home" => "index.md",
    ],
    repo="https://gitlab.com/greimel/StateGrids.jl/blob/{commit}{path}#L{line}",
    sitename="StateGrids.jl",
    authors="Fabian Greimel <fabgrei@gmail.com>",
    assets=String[],
)
