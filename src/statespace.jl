abstract type StateSpace end

struct EndogenousStateSpace{T0,T1,T2,T3,T4} <: StateSpace
  grids::T0
  grid::T1 # grid
  indices::T2
  linear_indices::T3
  size::T4
end

struct ExogenousStateSpace{T0,T1,T2,T3,T4,T5} <: StateSpace
  grids::T0
  grid::T1 # grid
  indices::T2
  linear_indices::T3
  size::T4
  mc::T5
  #dist::T6 # stationary distribution
end

grid(ss::StateSpace) = ss.grid
Base.size(ss::StateSpace) = ss.size
Base.length(ss::StateSpace) = prod(size(ss))
Base.keys(ss::StateSpace) = keys(grid(ss)[1])
dimension(ss::StateSpace) = length(size(ss))
linear_indices(ss::StateSpace) = ss.linear_indices

transition_matrix(exo::ExogenousStateSpace, args...) = transition_matrix(exo.mc, args...)

named_grid(exo::ExogenousStateSpace, args...) = named_grid(exo.mc, args...)

function EndogenousStateSpace(grids_nt::NamedTuple)
  keys_ = keys(grids_nt)
  
  size_ = Tuple(length.(collect(grids_nt)))
  
  grid0 = Iterators.product(grids_nt...)
  gridNT = NamedTuple{keys_}.(grid0)
  
  indices0 = Iterators.product([1:n for n in size_]...)
  indicesNT = NamedTuple{keys_}.(indices0)
  
  EndogenousStateSpace(grids_nt, gridNT, indicesNT, LinearIndices(size_), size_)
   
end

function ExogenousStateSpace(mcT::MarkovChainT, named_grids=[mcT.state_values])
  size = Tuple(length.(named_grids))
    
  grid = mcT.state_values
  
  indices0 = collect(Iterators.product([1:n for n in size]...))
  indicesNT = NamedTuple{keys(grid[1])}.(indices0)
  
  ExogenousStateSpace(named_grids, grid, indicesNT, LinearIndices(size), size, mcT)
end

function ExogenousStateSpace(vec_mc::Vector)
  grids = named_grid.(vec_mc)
  size = Tuple(length.(grids))
    
  if length(vec_mc) > 1
  mc = product(vec_mc...)
  else
    mc = vec_mc[1]
  end
  
  grid = named_grid(mc)
  
  indices0 = collect(Iterators.product([1:n for n in size]...))
  indicesNT = NamedTuple{keys(grid[1])}.(indices0)
  
  ExogenousStateSpace(grids, grid, indicesNT, LinearIndices(size), size, mc)
end

function add_name(mc::MarkovChain, name::Symbol)
  MarkovChain(mc.p, named_grid(mc.state_values, name))
end

function MarkovChain(p, state_values, name::Symbol)
  MarkovChain(p, named_grid(state_values, name))
end

## Vector of independent Markov chains x1, x2
function product_of_named_grids(grid1, grid2)
  [merge(s1, s2) for s2 in grid2 for s1 in grid1]
end

product_of_trans_matrices(trans1, trans2) = kron(trans2, trans1)
  
function product(mc1::MarkovChain, mc2::MarkovChain)
  combined_grid = product_of_named_grids(mc1.state_values, mc2.state_values)
  combined_trans_matrix = product_of_trans_matrices(mc1.p, mc2.p)
  MarkovChain(combined_trans_matrix, combined_grid)
end

function product(mc_vec...)
  #@show eltype(mc_vec)
  @assert eltype(mc_vec) <: MarkovChain
  if length(mc_vec) == 1
     return mc_vec[1]
  else
    mc12 = product(mc_vec[1], mc_vec[2])
  end
  if length(mc_vec) == 2
    return mc12
  else
    return product(mc12, mc_vec[3], mc_vec[4:end]...)
  end
end
