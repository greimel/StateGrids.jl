module StateGrids

import MarkovChainsX: MarkovChain, stationary_distributions

export MarkovChainT

# needed for bellman
export named_grid, transition_matrix#, length
# needed for cross-sectional distribution
# keys, size, linear_indices


include("markov-chains.jl")
include("statespace.jl")

export EndogenousStateSpace, ExogenousStateSpace, dimension, linear_indices, grid

end # module
