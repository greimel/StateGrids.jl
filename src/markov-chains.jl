import MarkovChainsX: MarkovChain

# ---------------------------------------------------------------------
# Methods for QuantEcon.MarkovChain
# ---------------------------------------------------------------------

named_grid(grid, name=:s) = NamedTuple{(name,)}.(Tuple.(Ref.(grid))) #TODO load from ExogenousStates i/o of copy-pasting
named_grid(grid::AbstractArray{<:NamedTuple}) = grid

transition_matrix(mc::MarkovChain, _=0) = mc.p
#transition_matrix(mc::Vector{<:MarkovChain}, t::Int) = transition_matrix(mc[t])
named_grid(mc::MarkovChain, args...) = named_grid(mc.state_values, args...)
named_grid(mc::MarkovChain, _::Int=0) = named_grid(mc.state_values)
#named_grid(mc::Vector{<:MarkovChain}, t::Int) = named_grid(mc[t])

# ---------------------------------------------------------------------
# Methods for (new) Finite Time MarkovChain
# ---------------------------------------------------------------------

struct MarkovChainT{T1,T2,T3}
  transition_matrix::T1 
  state_values::T2
  t_grid::T3
  
  function MarkovChainT(transition_matrix::T1, state_values::T2, t_grid::T3) where {T1<:AbstractMatrix, T2, T3}
    check_stochastic_matrix(transition_matrix)
    
    n = size(transition_matrix, 1)
    length(state_values) != n &&
      throw(DimensionMismatch("state_values should have $n elements"))
    
    return new{T1,T2,T3}(transition_matrix, state_values, t_grid)
  end
  
  function MarkovChainT(p_vec::T1, state_values::T2, t_grid::T3) where {T1<:AbstractVector{<:AbstractMatrix}, T2, T3}
  
    check_stochastic_matrix.(p_vec)
  
    length(p_vec) != length(t_grid) && throw(DimensionMismatch("Length of t_grid ($(length(t_grid))) and number of transition matrices ($(length(p_vec))) must be the same"))
  
    n = size.(p_vec, 1)
  
    any(n .!= n[1]) && throw(DimensionMismatch("transition matrices don't have the same size"))
  
    return new{T1,T2,T3}(p_vec, state_values, t_grid)
  end
end

function transition_matrix(mc::MarkovChainT{<:AbstractMatrix,<:Any,<:Any}, i_t::Int=0)
  1 <= i_t <= length(mc.t_grid) ? mc.transition_matrix : throw(BoundsError())
end
transition_matrix(mc::MarkovChainT{<:AbstractVector{<:AbstractMatrix},<:Any,<:Any}, i_t::Int) = mc.transition_matrix[i_t]

named_grid(mc::MarkovChainT) = named_grid(mc.state_values)

function named_grid(mc::MarkovChainT, i_t::Int)
  (1 <= i_t <= length(mc.t_grid)) || throw(BoundsError())
  
  merge.(Ref((i_t=i_t, t=mc.t_grid[i_t])), named_grid(mc.state_values))
end

function check_stochastic_matrix(P)
  n, m = size(P)
  
  n != m && throw(DimensionMismatch("Stochastic matrix must be square"))

  minimum(P) < 0 && throw(ArgumentError("Stochastic matrix must have nonnegative elements"))

  maximum(abs, sum(P, dims = 2) .- 1) > 5e-15 &&
            throw(ArgumentError("stochastic matrix rows must sum to 1"))
end
