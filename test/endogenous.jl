@testset "Endogenous state space" begin
  a_min = 0.0
  h_min = eps()
  
  a = LinRange(a_min, 7.5, 10)
  h = LinRange(h_min, 5.0, 15)

  endo = EndogenousStateSpace((a=a, h=h))
  
  @test size(endo) == (10, 15)
  @test length(endo) == 10 * 15
  @test keys(endo) == (:a, :h)
  @test endo.grid[1,1] == (a=a_min, h=h_min)
  @test endo.grid[5,7] == (a=a[5], h=h[7])
  @test endo.indices[3,14] == (a=3, h=14)
end
