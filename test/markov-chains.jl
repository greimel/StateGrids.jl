import MarkovChainsX: MarkovChain

@testset "MarkovChain" begin
  p0 = ones(5,5)/5
  sv0 = rand(5)
  sv1 = named_grid(sv0, :x)
  
  mc0 = MarkovChain(p0, sv0)
  mc1 = MarkovChain(p0, sv1)
  
  @test transition_matrix(mc0) == mc0.p
  @test transition_matrix(mc1) == mc1.p
  
  named_grid(mc0)
  named_grid(mc0, :rmmll)
  @test named_grid(mc1) == mc1.state_values
end

@testset "MarkovChainT" begin
  p0 = ones(5,5)/5
  sv0 = rand(5)
  sv1 = named_grid(sv0, :x)

  @testset "Constructors" begin
    MarkovChainT(ones(5,5)./5, sv0, 1:10)
    # rows don't sum to 1
    @test_throws ArgumentError MarkovChainT(ones(5,5), sv0, 1:10)
    # matrix not square
    @test_throws DimensionMismatch MarkovChainT(p0[1:4,:], sv0, 1:10)
    # dimensions of grid and matrix don't match
    @test_throws DimensionMismatch MarkovChainT(ones(4,4)./4, sv0, 1:10)
    
    MarkovChainT([ones(5,5)./5 for i in 1:10], sv0, 1:10)
    # age_grid and number of matrices doesn't match
    @test_throws DimensionMismatch MarkovChainT([ones(5,5)./5 for i in 1:10], sv0, 1:9)
    # matrices are of different size
    @test_throws DimensionMismatch MarkovChainT([ones(i,i)./i for i in 1:10], sv0, 1:10)
  end

  @testset "Accessors" begin
    mc00 = MarkovChainT(ones(5,5)./5, sv0, 1:10)
    mc01 = MarkovChainT(ones(5,5)./5, sv1, 1:10)
    
    @test all(transition_matrix(mc00, 9) .≈ 0.2)
    @test_throws BoundsError transition_matrix(mc00, 12)
    @test named_grid(mc00, 9)[5] == (i_t=9, t=9, s=sv0[5]) 
    @test named_grid(mc01, 9)[5] == (i_t=9, t=9, sv1[5]...)
    @test_throws BoundsError named_grid(mc00, 12)
    
    mc1 = MarkovChainT([ones(5,5)./5 for i in 1:10], sv1, 1:10)
    
    @test all(transition_matrix(mc1, 9) .≈ 0.2)
    @test_throws BoundsError transition_matrix(mc1, 12)
    @test named_grid(mc1, 9)[5] == (i_t=9, t=9, sv1[5]...)
    @test_throws BoundsError named_grid(mc1, 12)
  end
end

