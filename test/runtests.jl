using StateGrids
using Test

@testset "StateGrids.jl" begin
    include("endogenous.jl")
    include("markov-chains.jl")
end
