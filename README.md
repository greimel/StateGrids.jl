# StateGrids

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://greimel.gitlab.io/StateGrids.jl/dev)
[![Build Status](https://gitlab.com/greimel/StateGrids.jl/badges/master/build.svg)](https://gitlab.com/greimel/StateGrids.jl/pipelines)
[![Coverage](https://gitlab.com/greimel/StateGrids.jl/badges/master/coverage.svg)](https://gitlab.com/greimel/StateGrids.jl/commits/master)
